# Collect_Instagram
Collect tag, post ID and information from Instagram.


## User :
set up the Account list and your keyword.
Run three code step by step.
1. Tags: it will collect each address of those post which is in the tag of your keyword. Output as a list.
2. IDs: it will collect each ID of author in the list. Output as a list.
3. infor: it will collect information of those author. Output as a list.
![image](https://gitlab.com/MarkHere/Collect_Instagram/-/raw/main/Collect_IG_tag.gif)
![image](https://gitlab.com/MarkHere/Collect_Instagram/-/raw/main/Collect_IG_tag.png)

## Backstage : Python + Thread
Thread: to run 1~100 crawler for speed up the collecting task, and also avoid the IP blocking from website.  

